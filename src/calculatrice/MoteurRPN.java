package calculatrice;

import java.util.ArrayList;
import java.util.Stack;



public class MoteurRPN  {
	private static final int One = 1;      
	private static final int Two = 2;     
	private static final int Three = 3;  
	public Stack<String> op_stack = new Stack<>();        // Stocker la pile d'opérateurs
	public  ArrayList<String> origin = new ArrayList<>();     
	public  ArrayList<String> dest = new ArrayList<>();      
	private static Stack<String> operation = new Stack<>();
	Stack<Double> p = new Stack<Double> ();
	
	public void enregistrer(Double v) {
		p.push(v);
	}
	public int getOp(String str){
        
        switch(str){
        case "(":return Three;
        case "*":
        case "/":return Two;
        case "+":
        case "-":return One;
        case ")":return 0;
        
        default : return -1;
        
        }
        
    }
	public boolean isOp(String str1,String str2){
        return getOp(str1) > getOp(str2);   
    }
	
	public void stack_op(String op){
        
        //Déterminer si la pile actuelle est vide
        if(op_stack.isEmpty()){
            op_stack.push(op);
            return;
        }
        
        //juge si  (
        if("(".equals(op)){
            op_stack.push(op);
            return;
        }
        
        //détermine si  )
        if(")".equals(op)){
            String string = "";
            while(!"(".equals(string = op_stack.pop())){
          	  dest.add(string);
            }
            return;
        }
        
       //Si l'élément supérieur actuel est  (  Empilement direct
       if("(".equals(op_stack.peek())){
           op_stack.push(op);
           return;
       }
       
       //  Jugez la priorité avec le haut de la pile, > est true
       if(isOp(op, op_stack.peek())){
           op_stack.push(op);
           return;
       }
       
       if(!isOp(op, op_stack.peek())){
           dest.add(op_stack.pop());
           stack_op(op);   
       }
       
   }
	public  int calcul(String s1,String s2,String s3){
		         int a = Integer.parseInt(s2);
		         int b = Integer.parseInt(s1);
		         switch(s3){
		         case "+":return a+b;
		         case "-":return a-b;
		         case "*":return a*b;
		         case "/":return a/b;
		         default : return 0;
		         }
		     }
			
		
	}
