package testRPN;



import org.junit.Before;

import static org.junit.Assert.assertEquals;



import static org.junit.Assert.*;
import calculatrice.CalculatriceRPN;
import calculatrice.MoteurRPN;
import calculatrice.SaisieRPN;

import org.junit.Test;


public class testRPN {
	private String str1;
	private String str2;
	private String str3;
	
	
	
	@Before
	public void setup()
	{
		str1 = "2*(3+4)";
		str2 = "234+*";
		str3 = "2+2";
		
	}
	@Test
	public void verifielaformule() {
		CalculatriceRPN c = new CalculatriceRPN(str1);
		c.change();
		String str=c.toString();
		assertTrue(str.equals(str2));
	
		
	}

	@Test
	public void testentree() {
		SaisieRPN s =new SaisieRPN();
		String test = s.Saisie();
		assertEquals(str3,test);
	}
	@Test
	public void testtotal() {
		SaisieRPN s =new SaisieRPN();
		String test = s.Saisie();
		CalculatriceRPN c = new CalculatriceRPN(test);
		c.change();
		assertEquals(14,c.js_nbl());
	}
	@Test public void testformuentree() {
		SaisieRPN s =new SaisieRPN();
		String test = s.Saisie();
		CalculatriceRPN c = new CalculatriceRPN(test);
		c.change();
		String str =c.toString();
		assertTrue(str.equals(str2));
	}

	
	
}
